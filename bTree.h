#ifndef BTREE_H
#define BTREE_H

class bTree {
    private:
        struct node {
            int key;
            node* left = nullptr;
            node* right = nullptr;
            node* parent = nullptr;
        };
        node* root;
	void inOrder(node*);
	void insertNode(node *&);

    public:
	bTree()
	{
		root = nullptr;
	}
//	~bTree();
	void inOrderWalk();
        void insert(int k);
	void rm();
        bool search(int k);
        int min();
        int max();
	int successor(int k);
	void print();
};


#include "bTree.h"
#endif
