#include "bTree.h"
#include <stdio.h>

void bTree::inOrder(node* x)
{
	if (x != nullptr) {
		inOrder(x->left);
		printf(" %d ",x->key);
		inOrder(x->right);
	} 
}

void bTree::inOrderWalk()
{
	inOrder(root);
	printf("\n");
}

void bTree::insertNode(node*& z)
{

	node* y = nullptr;
	node* x = root;

	while (x != nullptr)
	{
		y = x;
		if (z->key < x->key) {
			x = x->left;
		} else {
			x = x->right;
		}
	}

	z->parent = y;

	if (y == nullptr) {
		root = z;
	} else if (z->key < y->key) {
		y->left = z;
	} else {
		y->right = z;
	}

}

void bTree::insert(int k)
{
	node* foo = new node;
	foo->key = k;
	insertNode(foo);
}

bool bTree::search(int k)
{
	if (root == nullptr) {
		return false;
	}

	node* cursor = root;

	while (cursor != nullptr)
	{
		if (k < cursor->key)
		{
			cursor = cursor->left;
		} else if (k > cursor->key) {
			cursor = cursor->right;
		} else if (k == cursor->key) {
			return true;
		}
	}

	return false;
}









